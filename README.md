# Docker compose with Django 4, Celery, Redis and Postgres

## Start

### Development

```
virtualenv venv 
# activate virtual environment
source venv/bin/activate

pip install -r requirements.txt
pip install -r requirements-dev.txt

sudo apt install redis
redis-cli

django-admin startproject backend
python manage.py startapp assignments
python manage.py runserver

python manage.py makemigrations
python manage.py migrate

python manage.py runserver

redis-cli
# please run in the backend directory (the same dir as runserver command)
cd backend
celery -A backend worker --loglevel=info --concurrency 1 -E

python manage.py runserver
http://127.0.0.1:8000/assignments/
# POST assignments
```

### Docker mode
```
# sudo apt update
# sudo apt install docker-compose-plugin
# sudo usermod -aG docker ${USER}
# su - ${USER}



# run in main project directory
sudo docker-compose build
docker-compose build -t myimage .

sudo docker-compose up
docker-compose --env-file ./backend/.env up

# When deploying to the production
sudo docker-compose up --build -d
sudo docker-compose down

docker volume ls
# remove volumes
docker-compose down --volumes

# check network
docker network ls
# Remove all unused networks
docker network prune

# login to the running containe
sudo docker exec -it <container_name> sh

http://192.168.0.111/assignments/

# Check .dockerignore
rsync -avn . /dev/shm --exclude-from .dockerignore

# restart docker
sudo service docker restart

docker stats
docker container ls
docker container stop <container_name>
docker container prune
docker inspect <container_name>
docker inspect <image_name>


# Delete all images
docker images
# docker rmi -f $(docker images -aq)

# Delete cache
docker system prune

docker volume ls
docker volume inspect docker_postgres_data
docker volume prune

docker exec -it 94c57 bash

# Redis WARNING Memory overcommit must be enabled
sudo sysctl "vm.overcommit_memory=1"
```


### Project structure
```
├── backend
│   ├── assignments
│   │   ├── admin.py
│   │   ├── apps.py
│   │   ├── __init__.py
│   │   ├── migrations
│   │   │   ├── 0001_initial.py
│   │   │   └── __init__.py
│   │   ├── models.py
│   │   ├── serializers.py
│   │   ├── tasks.py
│   │   ├── tests.py
│   │   ├── urls.py
│   │   └── views.py
│   ├── backend
│   │   ├── asgi.py
│   │   ├── celery.py
│   │   ├── __init__.py
│   │   ├── settings.py
│   │   ├── urls.py
│   │   └── wsgi.py
│   ├── db.sqlite3
│   └── manage.py
├── docker
│   ├── backend
│   │   ├── Dockerfile
│   │   ├── server-entrypoint.sh
│   │   └── worker-entrypoint.sh
│   └── nginx
│       └── default.conf
├── docker-compose.yml
├── LICENSE
├── README.md
└── requirements.txt
```





## Testing

### ApacheBench

```
sudo apt update
sudo apt install apache2-utils
ab -V
man ab

ab -n 100 -c 10 https://blog.sedicomm.com/
ab -n 10000 -c 100 http://127.0.0.1:8000/assignments/


ab -n 100 -c 10 -g out.data https://blog.sedicomm.com/
less out.data
apt install gnuplot
gnuplot
set terminal dumb
plot "out.data" using 9 w l

ab -n 100 -c 10 -g out.data https://www.sedicomm.com/
gnuplot
set terminal dumb
plot "out.data" using 9 w l

```
